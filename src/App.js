import React, { Component } from 'react';
import { getCoinPrice } from './coinmarketcap';

import {
    storePrice,
    getPrice,
    saveAccount,
    getAccount,
    doTrade,
    calculateAccount,
    CURRENCIES,
    calculateValue
} from './account';

import './App.css';

class App extends Component {

    constructor() {
        super();
        this.state = {
            prices: {
                bitcoin: undefined,
                ethereum: undefined,
                litecoin: undefined,
                dash: undefined
            },
            trade: {
                toAccount: 'usd',
                fromAccount: 'usd',
                toAmount: 0,
                fromAmount: 0
            }
        };

        //setInterval(() => this.setState({ ...this.state, flip: !this.state.flip}), 5000);
    }

    componentWillMount() {
        CURRENCIES.forEach(currency =>
            getCoinPrice(currency).then(data => {
                this.setState({
                    ...this.state,
                    prices: {
                        ...this.state.prices,
                        [currency]: data[0]
                    }
                });
            }));
    }

    updateTradeForm(field, value) {
        this.setState({
            trade: {
                ...this.state.trade,
                [field]: value
            }
        });
    }

    calculatePrice(toAccount, fromAccount, fromAmount) {
        const prices = this.state.prices;

        const toPrice = toAccount !== 'usd' ? prices[toAccount].price_usd : 1;
        const fromPrice = fromAccount !== 'usd' ? prices[fromAccount].price_usd : 1;

        return parseFloat(fromPrice) * parseFloat(fromAmount) / parseFloat(toPrice);
    }

    updateFields(field, value) {
        const trade = this.state.trade;
        if (trade.toAccount === trade.fromAccount) {
            this.setState({
                ...this.state,
                trade: {
                    ...this.state.trade,
                    toAmount: value,
                    fromAmount: value
                }
            });
        } else {
            this.setState({
                ...this.state,
                trade: {
                    ...this.state.trade,
                    toAmount: field === 'fromAmount'
                            ? this.calculatePrice(trade.toAccount, trade.fromAccount, value)
                            : value,
                    fromAmount: field === 'toAmount'
                            ? this.calculatePrice(trade.fromAccount, trade.toAccount, value)
                            : value
                }
            });
        }
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">coinpapertrade</h1>
                </header>

                <div className="panels">
                    <div className="panel">
                        <header>Current Account Value</header>
                        <span className="currencies">
                            <span className="currency">
                                <div className="currency-label">
                                    USD
                                </div>

                                <div className="currency-value">
                                    { `$${parseFloat(Number(calculateAccount('usd')))}` }
                                </div>
                            </span>

                            { CURRENCIES.map(currency => {
                                  return (
                                      <span key={ currency } className="currency">
                                          <div className="currency-label">
                                              { currency }
                                          </div>

                                          <div className="currency-value">
                                              { `${parseFloat(Number(calculateAccount(currency)), 2)}` }
                                          </div>
                                      </span>
                                  );
                            }) }

                            <span className="currency">
                                <span className="currency-label">
                                    Total Value (USD)
                                </span>

                                <span className="currency-value">
                                    { `$${calculateValue(getAccount(), this.state.prices)}`}
                                </span>
                            </span>

                        </span>
                    </div>

                    <div className="panel">
                        <header>Trade</header>

                        <form
                            className="trade-form"
                            onSubmit={ e => {
                                    e.preventDefault();
                                    doTrade(this.state.trade);
                                    this.setState({
                                        ...this.state,
                                        trade: {
                                            fromAccount: 'usd',
                                            toAccount: 'usd',
                                            fromAmount: 0,
                                            toAmount: 0
                                        },
                                        flip: !this.state.flip
                                    });
                            } }>

                            <div>
                                <label htmlFor="currency">
                                    To Currency
                                </label>
                                <select
                                    name="currency"
                                    onChange={ e => {
                                        this.updateTradeForm('toAccount', e.target.value);
                                        //this.updateFields('toAmount', 0);
                                    } }>
                                    { ['usd', ...CURRENCIES].map(currency =>
                                        (
                                            <option
                                                key={ currency }
                                                value={ currency }>
                                                { currency }
                                            </option>
                                        )
                                    )}
                                </select>
                            </div>

                            <div>
                                <label htmlFor="fromCurrency">
                                    From Currency
                                </label>
                                <select
                                    name="fromCurrency"
                                    onChange={ e => {
                                        this.updateTradeForm('fromAccount', e.target.value);
                                        // this.updateFields('fromAmount', 0);
                                    } }>
                                    { ['usd', ...CURRENCIES].map(currency =>
                                        (
                                            <option
                                                key={ currency }
                                                value={ currency }>
                                                { currency }
                                            </option>
                                        )
                                    )}
                                </select>
                            </div>

                            <div>
                                <label htmlFor="toAmount">
                                    Pay To
                                </label>
                                <input
                                    type="text"
                                    name="toAmount"
                                    onChange={ e => this.updateFields('toAmount', e.target.value) }
                                    value={ this.state.trade && this.state.trade.toAmount }
                                />
                            </div>

                            <div>
                                <label htmlFor="fromAmount">
                                    Pay From
                                </label>
                                <input
                                    type="text"
                                    name="fromAmount"
                                    onChange={ e => this.updateFields('fromAmount', e.target.value) }
                                    value={ this.state.trade && this.state.trade.fromAmount }
                                />
                            </div>

                            <div>
                                <input type="submit" value="Do It!" />
                            </div>
                        </form>
                    </div>

                    <div className="panel">
                        <header>Existing Trades</header>

                        <div className="trades">
                            <table>
                                <thead>
                                    <tr>
                                        <td>From Account</td>
                                        <td>To Account</td>
                                        <td>Amount Paid</td>
                                        <td>Amount Received</td>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        getAccount().map((line, i) => {
                                            return (
                                                <tr key={ line.fromAccount + line.toAccount + line.fromAmount + i  }>
                                                    <td>{ line.fromAccount }</td>
                                                    <td>{ line.toAccount }</td>
                                                    <td>{ line.fromAmount.toFixed( line.fromAccount === 'usd' ? 2 : 6) }</td>
                                                    <td>{ line.toAmount.toFixed( line.toAccount === 'usd' ? 2 : 6) }</td>
                                                </tr>
                                            );
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className="panel">
                        <header>Currencies</header>
                        <table>
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>USD Price</td>
                                    <td>BTC Price</td>
                                </tr>
                            </thead>

                            <tbody>
                                { CURRENCIES.map(currency => {
                                      const info = this.state.prices[currency];
                                      if (info) {
                                          return (
                                              <tr key={ 'currency' + info.name + info.price_usd }>
                                                  <td>{ info.name }</td>
                                                  <td>{ info.price_usd }</td>
                                                  <td>{ info.price_btc }</td>
                                              </tr>
                                          );
                                      }

                                      return null;
                                }) }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
