const CURRENCIES = ['bitcoin', 'ethereum', 'litecoin', 'dash'];

function _defaultAccount() {
    return [{
        fromAccount: undefined,
        toAccount: 'usd',
        fromAmount: 0.0,
        toAmount: 1000.0
    }];
}

function storePrice(ticker, data) {
    const info = {
        ticker,
        usd: data.price_usd
    };
    window.localStorage.setItem(`cpt-${ticker}`,
                                JSON.stringify(info));
}

function getPrice(ticker) {
    return window.localStorage.getItem(`cpt-${ticker}`);
}

function saveAccount(data) {
    window.localStorage.setItem('cpt-account',
                                JSON.stringify(data));
}

function getAccount() {
    const data = window.localStorage.getItem('cpt-account');
    if (data) {
        return JSON.parse(data);
    }

    return _defaultAccount();
}

function validateTrade({ fromAccount, toAccount, toAmount, fromAmount }) {
    const account = getAccount();
    const errors = [];
    if (fromAccount === toAccount) errors.push(['Cannot trade to and from the same account.']);

    const fromAccountValue = calculateAccount(fromAccount);

    if (fromAmount > fromAccountValue) errors.push(['You do not have enough funds.']);

    return errors;
}

function doTrade({ fromAccount, toAccount, toAmount, fromAmount }) {
    const account = getAccount();
    saveAccount([
        ...account,
        { fromAccount, toAccount,
          toAmount: parseFloat(toAmount),
          fromAmount: parseFloat(fromAmount) }
    ]);
}

function calculateAccount(ticker) {
    const account = getAccount();
    const credits = account
        .filter(line => line.toAccount === ticker)
        .reduce((mem, item) => mem + item.toAmount, 0);

    const debits = account
        .filter(line => line.fromAccount === ticker)
        .reduce((mem, item) => mem + item.fromAmount, 0);

    return (credits - debits).toFixed(ticker === 'usd' ? 2 : 6);
}

function calculateValue(data, prices) {
    return (CURRENCIES.reduce((mem, currency) => {
        const currencyValue = calculateAccount(currency);
        if (parseFloat(currencyValue) === 0) {
            return mem;
        }
        const usdPrice = prices[currency] ? prices[currency].price_usd : 0;
        return mem + (parseFloat(usdPrice) * parseFloat(currencyValue));
    }, 0) + parseFloat(calculateAccount('usd'))).toFixed(2);


    return data.reduce((mem, item) => {
        if (item.toAccount === 'usd') {
            return mem + item.toAmount;
        } else {
            const price = prices[item.toAccount];
            const usdPrice = price && price.price_usd || 0;
            return mem + (item.toAmount / parseFloat(usdPrice));
        }
    }, 0).toFixed(2);
}

export {
    storePrice,
    getPrice,
    saveAccount,
    getAccount,
    validateTrade,
    doTrade,
    calculateAccount,
    calculateValue,
    CURRENCIES
};
