function getCoinPrice(ticker) {
    return fetch(`https://api.coinmarketcap.com/v1/ticker/${ticker}/`)
        .then(resp => resp.json());
}

export {
    getCoinPrice
};
